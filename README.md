# Sublime Configure

## Description
- sublime text の設定ファイルのバージョン管理
- MyBuildSystem は python 3 用のビルド設定などを含みます
- kimulatex は日本語 latex 環境下でのテキストのハードラップを実現するためのなんちゃって自作パッケージ
- そのうち、各設定の補足なんかをここに書き足す予定
<!-- 
## Demo

## VS. 

## Requirement

## Usage
 -->

## Install
1. Please clone this repository to an arbitrary local directory. (ex. <UserHome>/Dropbox/Setup)
    - もちろん、zipダウンロードでok（英語で考えるの疲れた）
1. Create symbolic link
    - for linux:
        - `ln -sf ~/Dropbox/Setup/sublime-configure/{kimulatex,MyBuildSystem,User} ~/.config/sublime-text-3/Packages/`
    - for mac:
        - `ln -sf ~/Dropbox/Setup/sublime-configure/{kimulatex,MyBuildSystem,User} ~/Library/Application\ Support/Sublime\ Text\ 3/Packages/`
	- for windows:
        - `cd "%HOMEPATH%\AppData\Roaming\Sublime Text 3\Packages"`
        - `mklink /D "User" "%HOMEPATH%\Dropbox\sublime-configure\Packages\User"`
        - `mklink /D "MyBuildSystem" "%HOMEPATH%\Dropbox\sublime-configure\Packages\MyBuildSystem"`
        - `mklink /D "kimulatex" "%HOMEPATH%\Dropbox\sublime-configure\Packages\kimulatex"`
    
## Contribution

## Licence

## Author

[M-Kimuny](https://gitlab.com/M-Kimuny)


