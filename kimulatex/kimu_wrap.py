# coding: utf-8
import sublime
import sublime_plugin
import re
import unicodedata

try:
    import Default.comment as comment
except ImportError:
    import comment


# ZenkakuMoji = '[あ-んア-ン一-鿐。、．，（）「」｛｝【】『』［］]'
ZenkakuMoji = '[あ-んア-ン一-鿐]'
ZenkakuFull = '[あ-んア-ン一-鿐。、．，（）「」｛｝【】『』［］]'
ZenkakuKigo = '[。、．，（）「」｛｝【】『』［］]'
RangeDelimiters = ["\\end{", "\\begin{", "%"]


def is_zenkaku(char):
    return re.search(ZenkakuMoji, char) is not None


def is_zenkakuKigo(char):
    return re.search(ZenkakuKigo, char) is not None


def is_zenkakuFull(char):
    return re.search(ZenkakuFull, char) is not None


def width_kana(str):
    all = len(str)      # 全文字数
    zenkaku = count_zen(str)        # 全角文字数
    hankaku = all - zenkaku     # 半角文字数

    return zenkaku * 2 + hankaku


# count str length considering zenkaku
def count_zen(str):
    n = 0
    for c in str:
        wide_chars = u"WFA"
        eaw = unicodedata.east_asian_width(c)
        if wide_chars.find(eaw) > -1:
            n += 1
    return n + len(str)


class KimuWrapChangeWidthCommand(sublime_plugin.TextCommand):
    width_index = 0

    def run(self, edit):
        setting = sublime.load_settings('kimuLatex.sublime-settings')
        wrap_width_candidates = setting.get('wrap_width_candidates', [50, 80])
        self.width_index = (self.width_index + 1) % len(wrap_width_candidates)
        wrap_width = wrap_width_candidates[self.width_index]
        self.view.show_popup("change wrap width to {0}".format(wrap_width))
        setting.set('wrap_width', wrap_width)
        # sublime.save_settings('kimuLatex.sublime-settings')


class KimuWrapCommand(sublime_plugin.TextCommand):
    def _determineWrapRange(self):  # return lineNumberOfRangeStart, RangeEnd
        current_pos = self.view.sel()[0].a
        current_line = self.view.rowcol(current_pos)[0]

        lastpos = self.view.size()
        lastline = self.view.rowcol(lastpos)[0]

        # return value (set default value)
        lineNumberRangeStart = current_line + 1
        lineNumberRangeEnd = current_line - 1

        # Search start
        target_line = self.view.rowcol(current_pos)[0]
        detected = False
        while not detected and target_line >= 0:
            target_str = self.view.substr(self.view.line(
                self.view.text_point(target_line, 0)))
            # print(target_line, target_str, len(target_str.strip()) == 0, [
            # target_str.startswith(delimter) for delimter in RangeDelimiters])
            if len(target_str.strip()) == 0 or True in [target_str.startswith(delimter) for delimter in RangeDelimiters]:
                lineNumberRangeStart = target_line + 1
                detected = True
            target_line -= 1

        # Search end
        target_line = self.view.rowcol(current_pos)[0]
        detected = False
        while not detected and target_line <= lastline:
            target_str = self.view.substr(self.view.line(
                self.view.text_point(target_line, 0)))
            # print(target_line, target_str, len(target_str.strip()) == 0, [
            # target_str.startswith(delimter) for delimter in RangeDelimiters])
            if len(target_str.strip()) == 0 or True in [target_str.startswith(delimter) for delimter in RangeDelimiters]:
                lineNumberRangeEnd = target_line - 1
                detected = True
            target_line += 1

        # print("range", lineNumberRangeStart, lineNumberRangeEnd)
        return lineNumberRangeStart, lineNumberRangeEnd

    def _get_frontward_text(self, lineNumberRangeStart):
        current_pos = self.view.sel()[0].a
        current_line = self.view.rowcol(current_pos)[0]

        # get text on current line (before current position)
        forward_texts = []
        forward_texts.append(self.view.substr(sublime.Region(
            self.view.text_point(current_line, 0), current_pos)).strip("\n"))

        target_line = current_line - 1

        while target_line >= lineNumberRangeStart:
            forward_texts.append(
                self.view.substr(
                    self.view.full_line(self.view.text_point(target_line, 0)))
                .strip("\n"))
            target_line -= 1

        forward_texts.reverse()
        return forward_texts

    def _get_backward_text(self, lineNumberRangeStart):
        current_pos = self.view.sel()[0].a
        current_line = self.view.rowcol(current_pos)[0]

        # get text on current line (after current position)
        backward_texts = []
        backward_texts.append(
            self.view.substr(
                sublime.Region(current_pos,
                               self.view.text_point(current_line + 1, 0)
                               )
            ).strip("\n")
        )
        target_line = current_line + 1

        while target_line <= lineNumberRangeStart:
            backward_texts.append(
                self.view.substr(
                    self.view.full_line(self.view.text_point(target_line, 0)))
                .strip("\n")
            )
            target_line += 1

        return backward_texts

    def _wrap_single_line(self, working_line, input_line, wrap_width):
        output_lines = []
        # working_line = working_line.strip()
        # input_line = input_line.strip()

        # remove \n and join two line
        if len(working_line) > 0 and\
            len(input_line) > 0 and\
            (
                not is_zenkakuKigo(working_line[-1]) and
                not (is_zenkakuFull(working_line[-1]) and
                     is_zenkakuFull(input_line[0]))
        ):
            working_line += " "
        working_line += input_line

        # if too long split
        if count_zen(working_line) > wrap_width:
            delim_pos = len(working_line)
            pos = 1
            while not (count_zen(working_line[:pos]) > wrap_width and
                       delim_pos < len(working_line))\
                    and pos < len(working_line):
                if(working_line[pos] == " "): #スペースなら切っていい
                    delim_pos = pos
                elif pos < len(working_line) and\
                        is_zenkaku(working_line[pos]) and\
                        is_zenkaku(working_line[pos - 1]): # 全角文字（記号除く）同士の間は切っていい
                    delim_pos = pos
                elif pos < len(working_line) and\
                        is_zenkakuKigo(working_line[pos - 1]) and\
                        not is_zenkakuKigo(working_line[pos]): # 最後の全角記号の後は切ってもいい
                    delim_pos = pos
                pos += 1
            output_lines.append(working_line[:delim_pos])
            working_line = working_line[delim_pos:].lstrip()
            # for the case working_line is moreover too long...
            working_line, olines = self._wrap_single_line(
                working_line, "", wrap_width)
            output_lines.extend(olines)
        return working_line, output_lines

    def run(self, edit):
        print("\n=========Original Text Wrap=========")
        print("(suppose latex, English or Japanese)")
        print("(created Kimura Masatoshi on 2018/3/18)")
        print("(created Kimura Masatoshi on 2019/11/25)")

        # load params
        wrap_width = sublime.load_settings(
            'kimuLatex.sublime-settings').get('wrap_width', 50)
        cursor_pos = [0, 0]
        start, end = self._determineWrapRange()
        print("wrap range", start, end)

        if start > end:
            self.view.show_popup("no wrap")
            return
        ftexts = self._get_frontward_text(start)
        btexts = self._get_backward_text(end)
        print("frontward_texts")
        for li in ftexts:
            print("\t" + li)
        print("backward_texts")
        for li in btexts:
            print("\t" + li)

        working_line = ""

        # handle above line
        wrapted = []
        for fline in ftexts:
            working_line, output_lines = self._wrap_single_line(
                working_line, fline, wrap_width)
            wrapted.extend(output_lines)

        # handle current line
        cursor_pos = [len(wrapted), len(working_line)]
        if len(ftexts[-1]) > 0:
            working_line, output_lines\
                = self._wrap_single_line(working_line + btexts[0], "", wrap_width)
            wrapted.extend(output_lines)
            btexts = btexts[1:]

        # handle bellow line
        for bline in btexts:
            working_line, output_lines = self._wrap_single_line(
                working_line, bline, wrap_width)
            wrapted.extend(output_lines)

        wrapted.append(working_line)

        p_start = self.view.text_point(start, 0)
        p_end = self.view.text_point(end + 1, 0)
        original_text_region = sublime.Region(p_start, p_end)
        self.view.erase(edit, original_text_region)

        if len(wrapted) > 0:
            self.view.insert(edit, p_start, "\n".join(wrapted) + "\n")

        target = self.view.text_point(cursor_pos[0] + start, cursor_pos[1])
        self.view.sel().clear()
        self.view.sel().add(sublime.Region(target))
        self.view.show(target)

        print("======End Original Text Wrap=========\n")
        return
